var Set = require('jsclass/src/set').Set;
var uuid = require('node-uuid');
var SingleRace = require('./SingleRace');
var CupRace = require('./CupRace');


module.exports = Room;

// constants
Room.ROOM_SIZE = 2;

// enums
Room.RACE_TYPE_SINGLE_RACE              = "single_race";
Room.RACE_TYPE_CUP_RACE                 = "cup";

Room.ROOM_STATE_WAITING                 = 0;
Room.ROOM_STATE_INGAME                  = 1;
Room.ROOM_STATE_DONE                    = 2;

function Room(data, io) {
    this.io = io;
    this.playersSet = new Set();
    this.state = Room.ROOM_STATE_WAITING;
    this.id = uuid.v4();
    this.io_room = 'room-'+this.id;
    this.raceType = data.raceType;
    this.race = null;
}

Room.prototype.join = function (user) {

    var room = this;

    if (this.playersSet.count() >= Room.ROOM_SIZE){
        console.log(user.name+" cannot join room "+this.id+" (full)");
        return new Error("room full");
    }

    // add user to room
    this.playersSet.add(user);
    user.room = this;

    console.log(user.name+" joined room "+this.id+"  ("+this.playersSet.count()+"/"+Room.ROOM_SIZE+")");

    // join socket-io room
    user.socket.join(this.io_room);
    console.log(user.name+" joined socket-io room: '"+this.io_room+"'");

    // notify other users that a new user joined the room
    user.socket.broadcast.to(this.io_room).emit('room_user_joined', {id: user.name, displayName: user.friendlyName, avatarId: user.avatarId});

    // if room reached capacity, start session
    if (this.playersSet.count() >= Room.ROOM_SIZE){
        process.nextTick(function(){ // start game on next tick (execution cycle), so that we can send join_room rsp before game starts
            room.start();
        });
    }
};

Room.prototype.leave = function (user) {
    this.playersSet.remove(user);
    console.log(user.name+" left room "+this.id+"  ("+this.playersSet.count()+"/"+Room.ROOM_SIZE+")");
    user.room = null;

    if (this.race)
        this.race.leave(user);

    // leave socket.io room
    user.socket.leave(this.io_room);
    console.log(user.name+" left socket-io room: '"+this.io_room+"'");

    // notify other clients that user is leaving
    user.socket.broadcast.to(this.io_room).emit('room_user_left', {id: user.name});

    //if everyone leaves, destroy the room
    if (this.playersSet.count() <= 0)
        this.destroy();

};

Room.prototype.start = function () {
    console.log("Room "+this.id+" full; starting game");

    var room = this;
    this.state = Room.ROOM_STATE_INGAME;

    room.io.sockets.in(this.io_room).emit('room_ready', {roomId: room.id});

    if (room.race){
        console.error("Room.start: "+this.id+":   room.race should be null");
        this.destroy();
        return;
    }

    if (room.raceType == Room.RACE_TYPE_CUP_RACE){
        room.race = new CupRace(this.playersSet, this.io, room.io_room);
    }
    else {
        room.race = new SingleRace(this.playersSet, this.io);
    }

    room.race.start();
    room.race.on('end', function(){
        console.log("Room:  Received 'end' from SingleRace/Cup");
        room.race = null;
        // race (single or cup) ended
    });
    room.race.on('error', function(){
        console.log("Room:  Received 'error' from SingleRace/Cup");
        room.race = null;
        room.destroy();
    });
};

Room.prototype.isAvailable = function() {
  return this.state == Room.ROOM_STATE_WAITING;
};

Room.prototype.destroy = function(){
    var room = this;

    room.io.sockets.in(this.io_room).emit('room_end', {roomId: room.id});

    // remove each user from socket.io room
    this.playersSet.forEach(function(p){
        p.socket.leave(room.io_room);
    });

    // remove each user from list
    this.playersSet.forEach(function(p) {
        room.leave(p);
    });


    this.state = Room.ROOM_STATE_WAITING;
    this.onDestroy();
};

Room.prototype.onDestroy = function(onDestroy){
    this.onDestroy = onDestroy;
};

Room.prototype.userDetailsList = function () {
    return this.playersSet.map(function (p) {
        return {
            id: p.name,
            displayName: p.friendlyName,
            avatarId: p.avatarId
        };
    });
};



