module.exports = User;

function User(name, socket, friendlyName, avatarId){
    this.name = name;
    this.socket = socket;
    this.room = null;
    this.friendlyName = friendlyName;
    this.avatarId = avatarId;
}
