module.exports = SingleRace;

var Set = require('jsclass/src/set').Set;
var Race = require('./Race');
var MathGame = require('./MathGame');

function SingleRace(usersSet, io) {
    Race.call(this);
    this.io = io;
    this.usersSet = new Set(usersSet);
    this.game = null;
}

SingleRace.prototype = Object.create(Race.prototype);
SingleRace.prototype.constructor = SingleRace;

SingleRace.prototype.start = function () {

    var singleRace = this;

    if (this.game) {
        console.error("start() already called earlier on this instance of SingleRace");
        return;
    }

    var game = this.game = new MathGame(this.usersSet, this.io);

    var users = this.usersSet.toArray();
    var gameDataPayload = {gameDataArray: [game.getGameData()]};

    this.sendGameData(users, gameDataPayload).then(function () {
        game.start();
        game.on('end', function () {
            // game ended
            console.log("SingleRace:  Received 'end' from Game");
            singleRace.emit('end');
        });

    }, function (error) {
        // failed to send game_data
        console.error("Failed to send game_data and receive acks");
        singleRace.emit('error');

    })
};

SingleRace.prototype.stop = function () {
};

SingleRace.prototype.leave = function (user) {
    this.usersSet.remove(user);
};