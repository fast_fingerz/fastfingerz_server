var Game = require('./Game');

module.exports = MathGame;

function MathGame(usersSet, io){
    Game.call(this, usersSet, io);
}

MathGame.prototype = Object.create(Game.prototype);
MathGame.prototype.constructor = MathGame;

MathGame.prototype.getGameData = function(){
    return JSON.parse('{"PointsPerQuestion":100,"TargetPoints":'+this.TARGET_POINTS+',"GameType":0,"Questions":[{"fi":7,"si":7,"a":14,"o":0,"ic":true,"wd":false},{"fi":9,"si":2,"a":11,"o":0,"ic":true,"wd":false},{"fi":1,"si":8,"a":-3,"o":1,"ic":false,"wd":false},{"fi":0,"si":0,"a":-4,"o":1,"ic":false,"wd":false},{"fi":2,"si":5,"a":-3,"o":1,"ic":true,"wd":true},{"fi":7,"si":1,"a":6,"o":1,"ic":false,"wd":false},{"fi":9,"si":7,"a":13,"o":0,"ic":false,"wd":false},{"fi":0,"si":2,"a":0,"o":3,"ic":true,"wd":false},{"fi":0,"si":0,"a":2,"o":1,"ic":false,"wd":false},{"fi":1,"si":9,"a":9,"o":3,"ic":false,"wd":false},{"fi":2,"si":8,"a":14,"o":3,"ic":false,"wd":false},{"fi":5,"si":4,"a":23,"o":3,"ic":false,"wd":false},{"fi":4,"si":9,"a":-5,"o":1,"ic":true,"wd":false},{"fi":0,"si":9,"a":-12,"o":1,"ic":false,"wd":false},{"fi":1,"si":6,"a":6,"o":3,"ic":true,"wd":false}]}');
};

MathGame.prototype.getTargetPoints = function(){
    return this.TARGET_POINTS;
};

MathGame.prototype.TARGET_POINTS = 1000;