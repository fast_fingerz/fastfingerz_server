module.exports = CupRace;

var Set = require('jsclass/src/set').Set;
var MathGame = require('./MathGame');
var Race = require('./Race');
var async = require("async");

// constants
CupRace.NUM_GAMES_PER_CUP = 3;

function CupRace(usersSet, io, io_room) {
    Race.call(this);
    var cupRace = this;
    this.io = io;
    this.usersSet = new Set(usersSet);
    this.io_room = io_room;
    this.gameArray = [];
    this.cupStats = {};
    this.usersSet.forEach(function(u){ // structure to store cup stats for each user { user1: {}, user2: {}, etc }
        cupRace.cupStats[u.name] = {totalCupPoints: 0, position: -1};
    });
}
CupRace.prototype = Object.create(Race.prototype);
CupRace.prototype.constructor = CupRace;

CupRace.prototype.start = function () {

    var cupRace = this;
    var gameDataPayload = {gameDataArray: []};

    // create N game objects and fetch game data; send game data to user
    for (i = 0; i < CupRace.NUM_GAMES_PER_CUP; i++) {
        var game = new MathGame(cupRace.usersSet, cupRace.io);
        this.gameArray.push(game);
        var gameData = game.getGameData();
        gameDataPayload.gameDataArray.push(gameData);
    }

    this.sendGameData(this.usersSet.toArray(), gameDataPayload).then(function () {

        var i = 0;
        async.eachSeries(cupRace.gameArray, function(game, callback){
            i++;

            if (!cupRace.usersSet.size){ // no more users in race
                callback(new Error("CupRace: error starting game "+i+"; no more users in race"))
                return;
            }

            console.log("CupRace: starting race "+i);
            game.start();
            game.on('end', function (err, userStatsArray) {
                // game ended
                console.log("CupRace:  Received 'end' from Game; start next race");
                if (!err){
                    // compute cup stats; send cup_summary message

                    var cupSummaryUserArray = cupRace.updateCupStats(userStatsArray);

                    console.log("CupRace: sending 'cup_summary' message to all");
                    cupRace.io.sockets.in(cupRace.io_room).emit('cup_summary', {
                        userArray: cupSummaryUserArray
                    });
                }
                callback();
            });
        },
        function(err){
            if (err){
                // cup did not finish successfully
                cupRace.emit("error");
            }
            else{
                // cup finished successfully
                cupRace.emit("end");
            }
        });


    }, function (error) {
        // failed to send game_data
        console.error("CupRace: Failed to send game_data and receive acks");
        cupRace.emit('error');

    });
};

CupRace.prototype.stop = function () {
};

CupRace.prototype.leave = function (user) {
    //console.log("CupRace: "+user.name+" leaving");
    // remove user from all games
    for (var i=0; i<this.gameArray.length; i++){
        var game = this.gameArray[i];
        //console.log("CupRace: removing "+user.name+" from game "+game.id);
        game.leave(user);
    }
    if (user.name in this.cupStats) {
        delete this.cupStats[user.name]; // remove user from cupStats array
    }
    this.usersSet.remove(user); // remove user from usersSet
};


CupRace.prototype.updateCupStats = function (gameUserStatsArray) {

    /* update cup statistics for each user and return data structure like this:

    [
        {id: "test_0", totalCupPoints: 2, position: 5, prevPosition: -1},
        {id: "test_1", totalCupPoints: 3, position: 4, prevPosition: -1},
        {id: "test_2", totalCupPoints: 5, position: 3, prevPosition: -1},
        {id: "test_3", totalCupPoints: 7, position: 2, prevPosition: -1},
        {id: "test_4", totalCupPoints: 10, position: 1, prevPosition: -1}
    ]

    */
    var cupRace = this;

    var cupStatsArray = [];

    // update totalCupPoints in cupStats data structure for each user
    for (i=0; i<gameUserStatsArray.length; i++){
        var userStats = gameUserStatsArray[i];

        if (userStats.id in cupRace.cupStats){
            cupRace.cupStats[userStats.id].totalCupPoints += userStats.cupPoints;
        }
    }

    // create an array with all users in race containing {id, totalCupPoints, position, prevPosition}
    for (var id in cupRace.cupStats) {
        cupStatsArray.push({
            id: id,
            totalCupPoints: cupRace.cupStats[id].totalCupPoints,
            position: -1,
            prevPosition: cupRace.cupStats[id].position
        });
    }

    // calculate rankings for users (based on updated total cup points)
    cupStatsArray.sort(function(a,b){return b.totalCupPoints - a.totalCupPoints}); // sort users by totalCupPoints (descending)

    var ranking = 0;
    var prevUserPoints = null;
    for (i=0; i<cupStatsArray.length; i++){

        if (prevUserPoints != cupStatsArray[i].totalCupPoints){
            ranking++; // if 2 users have the same totalCupPoints, keep their cup ranking the same
        }

        cupStatsArray[i].position = ranking;
        cupRace.cupStats[cupStatsArray[i].id].position = ranking;
        prevUserPoints = cupStatsArray[i].totalCupPoints;
    }

    return cupStatsArray;
};