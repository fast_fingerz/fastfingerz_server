var assert = require('chai').assert;
var io = require("socket.io-client");
var async = require("async");

var socket = [];
var gameDataCB = [];

function validateUsersConnect(){
    return function(done){
        this.timeout(5000);
        async.forEach([0,1,2,3,4],function(i,callback){
            socket[i] = io.connect("http://localhost:3000", {
                transports: ['websocket'],
                query: "token=test_"+i +
                "&logintype=userid" +
                "&friendlyname=" + "FriendlyUser" + i +
                "&avatarid=1",
                'force new connection': true
            });
            assert.ok(socket[i]);
            socket[i].once('connect', function(){
                callback();
            });
        }, function(err){
            done();
        });
    };
}

function validateUsersJoin() {
    return function (done) {
        this.timeout(15000);
        async.parallel({
            allUsersShouldJoin: function (callback) {
                async.forEachOf(socket, function (s, i, joinDoneCallback) {
                    s.emit('join_room', {raceType: "cup"}, function (rspData) {
                        //console.log("test_"+i+" joined room");
                        assert.equal(rspData.err, '', "rspdata.err should be '';  rspData:" + JSON.stringify(rspData));
                        joinDoneCallback();
                    });
                }, function (err) {
                    callback();
                });
            },
            allUsersShouldGetRoomReady: function (callback) {
                async.forEachOf(socket, function (s, i, gotRoomReadyCallback) {
                    s.once('room_ready', function (data) {
                        //console.log("test_"+i+": got room_ready");
                        gotRoomReadyCallback();
                    });
                }, function(err){
                    callback();
                });
            },
            allUsersShouldGetGameData: function (callback) {
                async.forEachOf(socket, function (s, i, gotGameDataCallback) {
                    s.once('game_data', function (data, cb) {
                        //console.log("test_"+i+": got game_data");
                        gameDataCB[i] = cb;
                        gotGameDataCallback();
                    });
                }, function(err){
                    callback();
                });
            }
        }, function (err) {
            //console.log("done waiting for users to join");
            done();
        });
    };
}

function validateFirstGameShouldStart(){
    return function(done){
        async.forEachOf(socket,function(s,i,callback){
            s.once('game_start', function(){
                //console.log("test_"+i+":  got game_start");
                callback();
            });
            //console.log("test_"+i+": sending game_data ack");
            gameDataCB[i]('ok');

        }, function(err){
            //console.log("All users got game_start");
            done();
        });
    };
}

function validateUsersDisconnect() {
    return function(done){
        for (var i=0; i<socket.length; i++){
            socket[i].disconnect();
        }
        setTimeout(function(){done()},500);
    };
}

function validateGame(params){
    return function(done){
        this.timeout(5000);

        var userData = [];
        for (var username in params.testData){
            var i =  parseInt(username.replace("test_",""));
            userData[i] = params.testData[username];
        }

        // all users should send final scores
        for (var i in userData){
            socket[i].emit('score_final', userData[i].final_score);
        }

        async.parallel({
            wait_for_game_ends: function (doneTaskCB) {
                async.forEachOf(socket, function (s, i, callback) {
                    //var dataToPrint = null;
                    s.once('game_end', function (data) {
                        //if (!dataToPrint) {dataToPrint = data; console.log(dataToPrint)};
                        //console.log("game_end received by test_"+i+": "+JSON.stringify(data));
                        validateGameEnd(data);
                        //console.log("validated game_end received by test_" + i);
                        callback();
                    });
                }, function (err) {
                    doneTaskCB();
                });
            },
            wait_for_game_datas: function (doneTaskCB) {
                if (params.lastGame){
                    // last game; users should not get game_start (when game ends)
                    doneTaskCB();
                }
                else {
                    // users should each get game_start (when game ends)
                    async.forEachOf(socket, function (s, i, callback) {
                        s.once('game_start', function (data) {
                            //console.log("game_start received by test_" + i);
                            callback();
                        });
                    }, function (err) {
                        doneTaskCB();
                    });
                }
            },
            wait_for_cup_summary : function(doneTaskCB){
                async.forEachOf(socket, function (s, i, callback) {
                    s.once('cup_summary', function (data) {
                        validateCupSummary(data);
                        //console.log("cup_summary received by test_" + i);
                        callback();
                    });
                }, function (err) {
                    doneTaskCB();
                });
            }
        }, function (err) {
            done();
        });

        function validateGameEnd(data){
            assert(data.id, "game_end should have id field");
            assert(data.userArray, "game_end should have userArray field");
            assert(data.userArray.length, "game_end.userArray length should be valid");

            for (var username in params.testData) {
                // make sure all users' ranking/stats/etc is contained in game_end message
                var ranking_data = data.userArray.filter(containsUser(username));
                assert.equal(ranking_data.length,1);
                validate_user_ranking_data(ranking_data[0], params.testData[username]);
            }
        }

        function validate_user_ranking_data(rankingData, expectedUserData){
            assert.equal(rankingData.score, expectedUserData.final_score.score, "score in game_end for "+rankingData.id);
            assert.equal(rankingData.time, expectedUserData.final_score.time, "time in game_end for "+rankingData.id);
            assert.equal(rankingData.stats.qAnswered, expectedUserData.final_score.stats.qAnswered, "qAnswered in game_end for "+rankingData.id);
            assert.equal(rankingData.stats.qAnsweredCorrectly, expectedUserData.final_score.stats.qAnsweredCorrectly, "qAnsweredCorrectly in game_end for "+rankingData.id);
            assert.equal(rankingData.endStatus, expectedUserData.final_score.endStatus, "endStatus in game_end for "+rankingData.id);
            assert.equal(rankingData.finishedPosition, expectedUserData.expected.game.position, "finishedPosition in game_end for "+rankingData.id);
            assert.equal(rankingData.cupPoints, expectedUserData.expected.game.cupPoints, "cupPoints in game_end for "+rankingData.id);
        }

        function validateCupSummary(cupSummaryPayload){
            // make sure all users' cup stats are included in cup_summary message
            for (var username in params.testData){
                var filtered = cupSummaryPayload.userArray.filter(containsUser(username));
                assert.equal(filtered.length,1);

                var cupSummary = filtered[0];

                var expectedUserData = params.testData[username];
                assert.equal(cupSummary.totalCupPoints, expectedUserData.expected.cupSummary.totalCupPoints, "totalCupPoints in cup_summary for "+username);
                assert.equal(cupSummary.position,       expectedUserData.expected.cupSummary.position, "position in cup_summary for "+username);
                assert.equal(cupSummary.prevPosition,   expectedUserData.expected.cupSummary.prevPosition, "prevPosition in cup_summary for "+username);
            }
        }

        function containsUser(user){
            return function(obj,index,array){return obj.id && obj.id == user};
        }

    };
}

describe('scores in cup', function(){

    it('users should connect', validateUsersConnect());
    it('users should join', validateUsersJoin());
    it('first games should start', validateFirstGameShouldStart());
    it('validating first game', validateGame({testData:{
        test_0: { final_score: {score: 300, time: 2000, stats: {qAnswered: 5, qAnsweredCorrectly: 2}, endStatus: "DNFOutOfLives"},
            expected: {game:{position:5, cupPoints:2}, cupSummary:{totalCupPoints: 2, position: 5, prevPosition: -1}}
        },
        test_1: { final_score: {score: 400, time: 15000, stats: {qAnswered:6  ,qAnsweredCorrectly:3}, endStatus: "DNFOutOfTime"},
            expected: {game:{position:4, cupPoints:3}, cupSummary:{totalCupPoints: 3, position: 4, prevPosition: -1}}
        },
        test_2: { final_score: {score: 500, time: 15000, stats: {qAnswered:7  ,qAnsweredCorrectly:4}, endStatus: "DNFOutOfTime"},
            expected: {game:{position:3, cupPoints:5}, cupSummary:{totalCupPoints: 5, position: 3, prevPosition: -1}}
        },
        test_3: { final_score: {score: 1000, time: 6000, stats: {qAnswered:12 ,qAnsweredCorrectly:8}, endStatus: "targetScorereached"},
            expected: {game:{position:2, cupPoints:7}, cupSummary:{totalCupPoints: 7, position: 2, prevPosition: -1}}
        },
        test_4: { final_score: {score: 1000, time: 5000, stats: {qAnswered:15 ,qAnsweredCorrectly:10}, endStatus: "targetScorereached"},
            expected: {game:{position:1, cupPoints:10}, cupSummary:{totalCupPoints: 10, position: 1, prevPosition: -1}}
        }}}
    ));

    it('validating second game', validateGame({testData:{
            test_0: { final_score: {score: 300, time: 2000, stats: {qAnswered: 5, qAnsweredCorrectly: 2}, endStatus: "DNFOutOfLives"},
                expected: {game:{position:3, cupPoints:5}, cupSummary:{totalCupPoints: 7, position: 5, prevPosition: 5}}
            },
            test_1: { final_score: {score: 1000, time: 15000, stats: {qAnswered:6  ,qAnsweredCorrectly:3}, endStatus: "DNFOutOfTime"},
                expected: {game:{position:2, cupPoints:7}, cupSummary:{totalCupPoints: 10, position: 4, prevPosition: 4}}
            },
            test_2: { final_score: {score: 1000, time: 15000, stats: {qAnswered:7  ,qAnsweredCorrectly:4}, endStatus: "DNFOutOfTime"},
                expected: {game:{position:2, cupPoints:7}, cupSummary:{totalCupPoints: 12, position: 3, prevPosition: 3}}
            },
            test_3: { final_score: {score: 1000, time: 15000, stats: {qAnswered:12 ,qAnsweredCorrectly:8}, endStatus: "targetScorereached"},
                expected: {game:{position:2, cupPoints:7}, cupSummary:{totalCupPoints: 14, position: 2, prevPosition: 2}}
            },
            test_4: { final_score: {score: 1000, time: 5000, stats: {qAnswered:15 ,qAnsweredCorrectly:10}, endStatus: "targetScorereached"},
                expected: {game:{position:1, cupPoints:10}, cupSummary:{totalCupPoints: 20, position: 1, prevPosition: 1}}
            }}}
    ));

    it('validating third/last game', validateGame({lastGame:true, testData:{
            test_0: { final_score: {score: 10000, time: 2000, stats: {qAnswered: 5, qAnsweredCorrectly: 2}, endStatus: "DNFOutOfLives"},
                expected: {game:{position:1, cupPoints:10}, cupSummary:{totalCupPoints: 17, position: 3, prevPosition: 5}}
            },
            test_1: { final_score: {score: 8000, time: 15000, stats: {qAnswered:6  ,qAnsweredCorrectly:3}, endStatus: "DNFOutOfTime"},
                expected: {game:{position:2, cupPoints:7}, cupSummary:{totalCupPoints: 17, position: 3, prevPosition: 4}}
            },
            test_2: { final_score: {score: 8000, time: 15000, stats: {qAnswered:7  ,qAnsweredCorrectly:4}, endStatus: "DNFOutOfTime"},
                expected: {game:{position:2, cupPoints:7}, cupSummary:{totalCupPoints: 19, position: 2, prevPosition: 3}}
            },
            test_3: { final_score: {score: 7000, time: 6000, stats: {qAnswered:12 ,qAnsweredCorrectly:8}, endStatus: "DNFOutOfTime"},
                expected: {game:{position:3, cupPoints:5}, cupSummary:{totalCupPoints: 19, position: 2, prevPosition: 2}}
            },
            test_4: { final_score: {score: 7000, time: 5000, stats: {qAnswered:15 ,qAnsweredCorrectly:10}, endStatus: "DNFOutOfTime"},
                expected: {game:{position:3, cupPoints:5}, cupSummary:{totalCupPoints: 25, position: 1, prevPosition: 1}}
            }}}
    ));
    it('users should disconnect', validateUsersDisconnect());


});
