var assert = require("chai").assert;
var io = require("socket.io-client");
var async = require("async");

module.exports = FFTester;

function FFTester(params) {
    this.socket1 = null;
    this.socket2 = null;
    this.raceType = (params && params.raceType) || "single_race";
    this.expectedNumGamesInCup = (params && params.expectedNumGamesInCup) || 3;
    this.isCup = this.raceType == "cup";
    this.gameIndex = 0; // if we are in a cup, which game is it in the series 0,1,2,etc
}

FFTester.prototype.cleanup = function () {

    var that = this;

    return function (done) {
        if (that.socket1) {
            that.socket1.disconnect();
        }
        if (that.socket2) {
            that.socket2.disconnect();
        }
        setTimeout(function () {
            done()
        }, 500);
    }

};


FFTester.prototype.testUser1Connect = function () {
    var that = this;

    return function (done) {
        that.socket1 = io.connect("http://localhost:3000", {
            transports: ['websocket'],
            query: "token=test_1" +
            "&logintype=userid" +
            "&friendlyname=" + "FriendlyUser1" +
            "&avatarid=1",
            'force new connection': true
        });

        that.socket1.once('connect', function () {
            done();
        });
    }
};

FFTester.prototype.testUser2Connect = function () {
    var that = this;

    return function (done) {
        that.socket2 = io.connect("http://localhost:3000", {
            transports: ['websocket'],
            query: "token=test_2" +
            "&logintype=userid" +
            "&friendlyname=" + "FriendlyUser2" +
            "&avatarid=1",
            'force new connection': true
        });

        that.socket2.once('connect', function () {
            done();
        });
    }
};

FFTester.prototype.testUser1Join = function () {
    var that = this;

    return function (done) {
        var raceType = that.isCup? "cup" : "single_race";
        join_room_payload = {raceType: raceType};
        that.socket1.emit('join_room', join_room_payload, function (rspData) {
            assert.equal(rspData.err, '', "rspdata.err should be '';  rspData:" + JSON.stringify(rspData));
            assert.equal(rspData.data.users.length, 0, 'should be 0 users in room;  rspData:' + JSON.stringify(rspData));
            done();
        });
    }
};


FFTester.prototype.testUser2JoinAfterUser1 = function () {
    var that = this;
    var user1GotUser2JoinNotification = false;
    var user1GotRoomReady = false;
    var user2GotRoomReady = false;

    that.gameIndex = 0;

    return function (done) {
        async.parallel({
            join_room: function (callback) {
                var raceType = that.isCup? "cup" : "single_race";
                join_room_payload = {raceType: raceType};
                that.socket2.emit('join_room', join_room_payload, function (rspData) {
                    assert.equal(rspData.err, '', "rspData.err should be '';  rspData:" + JSON.stringify(rspData));
                    assert.equal(rspData.data.users.length, 1, 'should be 1 user in room;  rspData:' + JSON.stringify(rspData));
                    callback(null, rspData);
                });
            },
            user1_should_be_notified: function (callback) {
                that.socket1.once('room_user_joined', function (data) {
                    user1GotUser2JoinNotification = true;
                    callback(null, data);
                });
            },
            user1_should_get_room_ready: function (callback) {
                that.socket1.once('room_ready', function (data) {
                    user1GotRoomReady = true;
                    callback(null, data);
                });
            },
            user2_should_get_room_ready: function (callback) {
                that.socket2.once('room_ready', function (data) {
                    user2GotRoomReady = true;
                    callback(null, data);
                });
            },
            user1_should_get_game_data: function (callback) {
                that.socket1.once('game_data', function (data, cb) {
                    // validate that game_data comes after user1 was notified about user2 joining room and room_ready
                    assert(user1GotUser2JoinNotification);
                    assert(user1GotRoomReady);
                    assert(data.gameDataArray);
                    if (that.isCup) {
                        assert.equal(data.gameDataArray.length, that.expectedNumGamesInCup);
                    }
                    user1GameDataCb = cb;
                    callback(null, data);
                });
            },
            user2_should_get_game_data: function (callback) {
                that.socket2.once('game_data', function (data, cb) {
                    // validate that game_data comes after user2 gets room_ready
                    assert(user2GotRoomReady);
                    user2GameDataCb = cb;
                    assert(data.gameDataArray);
                    if (that.isCup) {
                        assert.equal(data.gameDataArray.length, that.expectedNumGamesInCup);
                    }
                    callback(null, data);
                });
            }
        }, function (err, results) {
            assert.equal(JSON.stringify(results.user1_should_get_game_data), JSON.stringify(results.user2_should_get_game_data), 'game_data for user1 and user2 should match');
            done(err);
        });
    }
};

FFTester.prototype.testAfterAckingGameDataGameShouldStart = function (delay) {
    var that = this;

    return function (done) {
        this.timeout(10000);

        var user1GameDataAckSent = false;
        var user2GameDataAckSent = false;


        assert(user1GameDataCb);
        assert(user2GameDataCb);

        user1GameDataCb("ok");
        user1GameDataAckSent = true;

        setTimeout(function () {
            user2GameDataCb("ok");
            user2GameDataAckSent = true;
        }, 2000);

        // game should start
        async.parallel({
            user1_gets_game_start: function (callback) {
                that.socket1.once('game_start', function (data) {
                    assert(user1GameDataAckSent);
                    assert(user2GameDataAckSent);
                    callback(null, data);
                });
            },
            user2_gets_game_start: function (callback) {
                that.socket2.once('game_start', function (data) {
                    assert(user1GameDataAckSent);
                    assert(user2GameDataAckSent);
                    callback(null, data);
                });
            }
        }, function (err, results) {
            done(err);
        });
    }

};

FFTester.prototype.testUser1GameStatusUpdate = function (delay) {
    var that = this;

    return function (done) {
        setTimeout(function () {
            that.socket2.once('game_status_update', function (data) {
                assert.equal(data.type, "score");
                assert.equal(data.payload, 1);
                assert.equal(data.from, "test_1");
                done();
            });
            that.socket1.emit('game_status_update_req', {type: "score", payload: 1});
        }, delay);
    }
};

FFTester.prototype.testUser1GameStatusUpdate = function (score, delay) {
    var that = this;

    return function (done) {
        setTimeout(function () {
            that.socket2.once('game_status_update', function (data) {
                assert.equal(data.type, "score");
                assert.equal(data.payload, score);
                assert.equal(data.from, "test_1");
                done();
            });
            that.socket1.emit('game_status_update_req', {type: "score", payload: score});
        }, delay);
    }
};

FFTester.prototype.testUser2GameStatusUpdate = function (score, delay) {
    var that = this;

    return function (done) {
        setTimeout(function () {
            that.socket1.once('game_status_update', function (data) {
                assert.equal(data.type, "score");
                assert.equal(data.payload, score);
                assert.equal(data.from, "test_2");
                done();
            });
            that.socket2.emit('game_status_update_req', {type: "score", payload: score});
        }, delay);
    }
};


FFTester.prototype.testGameShouldEndAfter1and2SendFinalScores = function (scores) {
    var that = this;

    return function (done) {
        this.timeout(10000);


        var user1FinalScoreSent = false;
        var user2FinalScoreSent = false;
        var user1GotNextGameStart = false;
        var user2GotNextGameStart = false;
        async.parallel({
            user1_gets_game_end: function (callback) {
                that.socket1.once('game_end', function (data) {
                    assert(user1FinalScoreSent);
                    assert(user2FinalScoreSent);

                    var user1_ranking_data = data.userArray.filter(containsUser('test_1'));
                    assert.equal(user1_ranking_data.length,1);
                    assert.equal(user1_ranking_data[0].score, scores.user1);

                    var user2_ranking_data = data.userArray.filter(containsUser('test_2'));
                    assert.equal(user2_ranking_data.length,1);
                    assert.equal(user2_ranking_data[0].score, scores.user2);
                    callback(null, data);
                });
            },
            user2_gets_game_end: function (callback) {
                that.socket2.once('game_end', function (data) {
                    assert(user1FinalScoreSent);
                    assert(user2FinalScoreSent);

                    var user1_ranking_data = data.userArray.filter(containsUser('test_1'));
                    assert.equal(user1_ranking_data.length,1);
                    assert.equal(user1_ranking_data[0].score, scores.user1);

                    var user2_ranking_data = data.userArray.filter(containsUser('test_2'));
                    assert.equal(user2_ranking_data.length,1);
                    assert.equal(user2_ranking_data[0].score, scores.user2);

                    callback(null, data);
                });
            },
            user1_send_final_score: function (callback) {
                setTimeout(function () {
                    that.socket1.emit('score_final', {score: scores.user1, time:25, stats:{qAnswered: 5, qAnsweredCorrectly: 3}, endStatus: "DNFOutOfLives" });
                    user1FinalScoreSent = true;
                    callback();
                }, getRandomInt(100, 500));
            },
            user2_send_final_score: function (callback) {
                setTimeout(function () {
                    that.socket2.emit('score_final', {score: scores.user2, time:50, stats:{qAnswered: 3, qAnsweredCorrectly: 2}, endStatus: "DNFOutOfLives" });
                    user2FinalScoreSent = true;
                    callback();
                }, getRandomInt(100, 500));
            },
            user1_gets_game_start:function(callback){ // if in cup, should also get game_start for next game
                if (that.isCup){
                    that.socket1.once('game_start',function(data){
                        user1GotNextGameStart = true;
                    });
                    setTimeout(function(){
                        if (that.gameIndex <= that.expectedNumGamesInCup-2){
                            assert.ok(user1GotNextGameStart);
                        }else{
                            assert.notOk(user1GotNextGameStart);
                        }
                        callback();
                    },5000);
                }else{
                    callback();
                }

            },
            user2_gets_game_start:function(callback){
                if (that.isCup){
                    that.socket2.once('game_start',function(data){
                        user2GotNextGameStart = true;
                    });
                    setTimeout(function(){
                        if (that.gameIndex <= that.expectedNumGamesInCup-2){
                            assert.ok(user2GotNextGameStart);
                        }else{
                            assert.notOk(user2GotNextGameStart);
                        }
                        callback();
                    },5000);
                }else{
                    callback();
                }
            }
        }, function (err, results) {
            if (that.isCup) {
                that.gameIndex++;
            }
            done(err);
        });
    }

    function containsUser(user){
        return function(obj,index,array){return obj.id && obj.id == user};
    }
};


FFTester.prototype.testUser1AcksGameData2DoesNotAck = function () {
    var that = this;

    return function (done) {
        this.timeout(15000);

        var user1GotGameStart = false;
        var user2GotGameStart = false;
        var user1GotRoomEnd = false;
        var user2GotRoomEnd = false;

        assert(user1GameDataCb);
        assert(user2GameDataCb);


        user1GameDataCb("ok");
        user1GameDataAckSent = true;

        that.socket1.once('game_start', function (data) {
            user1GotGameStart = true;
        });

        that.socket2.once('game_start', function (data) {
            user2GotGameStart = true;
        });

        that.socket1.once('room_end', function (data) {
            user1GotRoomEnd = true;
        });

        that.socket2.once('room_end', function (data) {
            user2GotRoomEnd = true;
        });

        setTimeout(function () {
            assert.notOk(user1GotGameStart);
            assert.notOk(user2GotGameStart);
            assert.ok(user1GotRoomEnd);
            assert.ok(user2GotRoomEnd);
            done();
        }, 7000);
    }
};

FFTester.prototype.testUser1AcksGameData2Disconnects = function () {
    var that = this;

    return function (done) {
        this.timeout(15000);

        var user1GotGameStart = false;
        var user2GotGameStart = false;
        var user1GotRoomEnd = false;
        var user2GotRoomEnd = false;

        assert(user1GameDataCb);
        assert(user2GameDataCb);


        user1GameDataCb("ok");
        user1GameDataAckSent = true;


        async.parallel({
                disconnectUser2: function (callback) {
                    setTimeout(function () {
                        that.socket2.disconnect();
                        callback();
                    }, 2000)
                },
                waitForUser1GameStart: function (callback) {
                    that.socket1.once('game_start', function (data) {
                        user1GotGameStart = true;
                        callback();
                    });
                }
            },
            function (err, results) {
                done();
            }
        );
    }
};

FFTester.prototype.testUser1AcksThenDcThen2Dc = function () {
    var that = this;

    return function (done) {
        this.timeout(15000);

        var user1GotGameStart = false;
        var user2GotGameStart = false;
        var user1GotRoomEnd = false;
        var user2GotRoomEnd = false;

        assert(user1GameDataCb);
        assert(user2GameDataCb);

        user1GameDataCb("ok");

        setTimeout(function(){
            that.socket1.disconnect();
        },2000);

        setTimeout(function(){
            that.socket2.disconnect();
        },4000);


        setTimeout(function () {
            assert.notOk(user1GotGameStart);
            assert.notOk(user2GotGameStart);
            done();
        }, 6000);

        that.socket1.once('game_start', function (data) {
            user1GotGameStart = true;
        });

        that.socket2.once('game_start', function (data) {
            user2GotGameStart = true;
        });

    }
};