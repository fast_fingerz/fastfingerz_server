var FFTester = require('./FFTester');
var assert = require('chai').assert;

getRandomInt = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

describe('single game; 1 acks game_data then disconnects, 2 disconnects; game should fail to start', function () {

    var fftester = new FFTester();

    it('1 should connect', fftester.testUser1Connect());
    it('2 should connect', fftester.testUser2Connect());
    it('1 should join', fftester.testUser1Join());
    it('2 should join after user 1 and should get game data', fftester.testUser2JoinAfterUser1());
    it('1 acks then disconnects; 2 disconnects; game should not start', fftester.testUser1AcksThenDcThen2Dc());

    after(fftester.cleanup());
});


describe('single game', function () {

    var fftester = new FFTester();

    it('1 should connect', fftester.testUser1Connect());
    it('2 should connect', fftester.testUser2Connect());
    it('1 should join', fftester.testUser1Join());
    it('2 should join after user 1 and should get game data', fftester.testUser2JoinAfterUser1());
    it('after 1 and 2 ack game_data, game should start', fftester.testAfterAckingGameDataGameShouldStart());

    it('1 game status update should be rcvd by 2', fftester.testUser1GameStatusUpdate(2, 0));
    it('1 game status update should be rcvd by 2', fftester.testUser1GameStatusUpdate(4, getRandomInt(100, 500)));
    it('1 game status update should be rcvd by 2', fftester.testUser1GameStatusUpdate(6, getRandomInt(100, 500)));

    it('2 game status update should be rcvd by 1', fftester.testUser2GameStatusUpdate(5, 0));
    it('2 game status update should be rcvd by 1', fftester.testUser2GameStatusUpdate(10, getRandomInt(100, 500)));
    it('2 game status update should be rcvd by 1', fftester.testUser2GameStatusUpdate(15, getRandomInt(100, 500)));

    it('after 1 and 2 send final scores, the game should end', fftester.testGameShouldEndAfter1and2SendFinalScores({
        user1: 8,
        user2: 20
    }));

    after(fftester.cleanup());

});


describe('single game; one user does not ack game_data fast', function () {

    var fftester = new FFTester();

    it('1 should connect', fftester.testUser1Connect());
    it('2 should connect', fftester.testUser2Connect());
    it('1 should join', fftester.testUser1Join());
    it('2 should join after user 1 and should get game data', fftester.testUser2JoinAfterUser1());
    it('1 acks; 2 does not ack; game should fail to start', fftester.testUser1AcksGameData2DoesNotAck());

    after(fftester.cleanup());
});


describe('single game; 1 acks game_data, 2 disconnects', function () {

    var fftester = new FFTester();

    it('1 should connect', fftester.testUser1Connect());
    it('2 should connect', fftester.testUser2Connect());
    it('1 should join', fftester.testUser1Join());
    it('2 should join after user 1 and should get game data', fftester.testUser2JoinAfterUser1());
    it('1 acks; 2 disconnects; game should start with just 1', fftester.testUser1AcksGameData2Disconnects());

    after(fftester.cleanup());
});

describe('cup', function () {

    var fftester = new FFTester({raceType: "cup", expectedNumGamesInCup: 3});

    it('1 should connect', fftester.testUser1Connect());
    it('2 should connect', fftester.testUser2Connect());
    it('1 should join', fftester.testUser1Join());
    it('2 should join after user 1 and should get game data', fftester.testUser2JoinAfterUser1());
    it('after 1 and 2 ack game_data, game should start', fftester.testAfterAckingGameDataGameShouldStart());

    it('1 game status update should be rcvd by 2', fftester.testUser1GameStatusUpdate(2, getRandomInt(100, 500)));
    it('2 game status update should be rcvd by 1', fftester.testUser2GameStatusUpdate(5, getRandomInt(100, 500)));
    it('after 1 and 2 send final scores, the game should end', fftester.testGameShouldEndAfter1and2SendFinalScores({
        user1: 2,
        user2: 5
    }));

    it('1 game status update should be rcvd by 2', fftester.testUser1GameStatusUpdate(4, getRandomInt(100, 500)));
    it('2 game status update should be rcvd by 1', fftester.testUser2GameStatusUpdate(10, getRandomInt(100, 500)));
    it('after 1 and 2 send final scores, the game should end', fftester.testGameShouldEndAfter1and2SendFinalScores({
        user1: 4,
        user2: 10
    }));

    it('1 game status update should be rcvd by 2', fftester.testUser1GameStatusUpdate(6, getRandomInt(100, 500)));
    it('2 game status update should be rcvd by 1', fftester.testUser2GameStatusUpdate(15, getRandomInt(100, 500)));
    it('after 1 and 2 send final scores, the game should end', fftester.testGameShouldEndAfter1and2SendFinalScores({
        user1: 6,
        user2: 15
    }));

    after(fftester.cleanup());
});


describe('invalid raceType', function () {

    var fftester = new FFTester();

    it('1 should connect', fftester.testUser1Connect());
    it('2 should connect', fftester.testUser2Connect());
    it('join should fail when invalid raceType is used', function(done){
        fftester.socket1.emit('join_room', {raceType:'blahRace'}, function(rspData){
            assert(rspData.err, "err should indicate join_room failed");
            done();
        })
    });

    after(fftester.cleanup());
});






