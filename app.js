var path = require('path');

var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var game_server = require('./game_server.js')(io);
var port = process.env.PORT || 3000;

server.listen(port);

if (app.get('env') === 'development') {
    app.locals.pretty = true;
}
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(require('express').static(__dirname + '/test'));

app.get('/', function(req,res) {
    res.render('index');
});