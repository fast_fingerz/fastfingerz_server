var User = require('./User');
var Room = require('./Room');
var Set = require('jsclass/src/set').Set;

exports = module.exports = createGameServer;

// module variables
var userTable = new Object();
var roomSet = new Set();

function createGameServer(io) {

    io.use(authorizeUser());

    io.on('connection', function (socket) {

        //todo: update this once enabling facebook/googleplus/etc
        var userName = socket.handshake.query.token;

        //has user already logged in?
        if (userName in userTable) {
            console.error(userName + " is already logged in.  close connection");
            socket.disconnect();
            return;
        }

        var friendlyName = socket.handshake.query.friendlyname || userName;
        var avatarId = socket.handshake.query.avatarid || 0;

        // add user to userTable
        console.log('add ' + userName + ' to user table');
        var user = userTable[userName] = new User(userName,socket,friendlyName,avatarId);

        socket.on('disconnect', function () {
            if (user.room) // if user is in a room, leave it
                user.room.leave(user);

            console.log('remove ' + user.name + ' from user table');
            delete userTable[user.name];
        });

        socket.on('message', function (msg) {
            console.log("Received from '"+user.name+"':    "+msg);
            socket.send(msg.toUpperCase());
        });

        socket.on('join_room',function(data, cb) {

            var room = null;

            if (user.room) { // is user already in a room?
                cb({err: "Already in room"});
                return;
            }

            data.raceType = data.raceType || Room.RACE_TYPE_SINGLE_RACE;
            if (!(data.raceType == Room.RACE_TYPE_CUP_RACE || data.raceType == Room.RACE_TYPE_SINGLE_RACE)){
                cb({err: "Invalid raceType"});
                return;
            }


            // is there a room available ?
            room = roomSet.find(function(r){
                return r.isAvailable() && (r.raceType == data.raceType);
            });

            if (room == null){ // no available room was found; create a new room

                var room_params = data;

                room = new Room(room_params, io);
                console.log("Could not find a room available; createa a new one: room="+room.id);
                room.onDestroy(function(){
                    console.log("Remove room "+room.id+" from roomSet");
                    roomSet.remove(room);
                });
                roomSet.add(room);
            }

            var usersInRoom = room.userDetailsList();

            // join room
            var ret = room.join(user);
            if (ret instanceof Error) {
                cb({err: ret.toString()});
                return;
            }

            cb({err: "", data: {users:usersInRoom, roomId: room.id}});
        });

        socket.on('leave_room', function(data, cb){

            if (!user.room) { // user is not in any room
                cb({err:"User is not in any room"});
                return;
            }

            // user is in a room...leave it
            roomId = user.room.id;
            user.room.leave(user);
            cb({err:"", roomId: roomId});

        });
    });

    function authorizeUser() {
        //todo: use token authorization
        return function (socket, next) {

            if (!socket.handshake.query)
                return next(new Error("querystring missing"));

            if (!socket.handshake.query.token)
                return next(new Error("token missing in querystring"));

            if (!socket.handshake.query.logintype)
                return next(new Error("logintype missing"));

            if (!(socket.handshake.query.logintype == "userid"))
                return next(new Error("unsupported login type '"+socket.handshake.query.logintype+"'.  currently only 'userid' supported"));

            var userName = socket.handshake.query.token;

            if (userName.substring(0, 4) != "test") { // any user that starts with 'test' is valid
                console.log("user '"+userName+"' not allowed");
                return next(new Error("user not allowed"));
            }

            console.log("user: '"+userName+"' authenticated.");
            next(); // user authenticated
        };
    }
}