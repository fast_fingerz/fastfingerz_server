var util = require("util");
var EventEmitter = require("events").EventEmitter;
var uuid = require('node-uuid');
var Set = require('jsclass/src/set').Set;
var Q = require('q');

module.exports = Game;

// constants
Game.GUARD_TIMEOUT = 60000; // if game doesn't finish in this much time, force it to end

Game.GAME_STATE_INIT = 0;
Game.GAME_STATE_ACTIVE = 1;
Game.GAME_STATE_DONE = 2;

function Game(usersSet, io) {
    EventEmitter.call(this);
    this.gameUserDataSet = new Set();
    this.id = uuid.v4();
    this.io = io;
    this.io_room = 'game-' + this.id;
    this.timer = null;
    this.state = Game.GAME_STATE_INIT;
    this.userStatsArray = [];
    console.log("Set game " + this.id + " state to " + this.state);

    var game = this;

    usersSet.forEach(function (u) {

        var user = u;
        var gameUserData = {
            user: user,
            score:0,
            time:-1,
            qAnswered:0,
            qAnsweredCorrectly:0,
            endStatus:"DNFOutOfTime",
            gotFinalScore: false
        };
        var gameUserDataSet = game.gameUserDataSet;

        gameUserDataSet.add(gameUserData);

        user.socket.once('disconnect', function () {
            game.leave(user);
        });
    });
}
util.inherits(Game, EventEmitter);

Game.prototype.start = function () {
    var game = this;

    if (this.state != Game.GAME_STATE_INIT)
        return;

    game.state = Game.GAME_STATE_ACTIVE;
    console.log("Set game " + this.id + " state to " + this.state);

    game.gameUserDataSet.forEach(function (gameUserData) {

        var user = gameUserData.user;

        // make each user join socket.io room
        user.socket.join(game.io_room);
        console.log(user.name + " joined socket-io room: '" + game.io_room + "'");

        user.socket.on('game_status_update_req', function (data) {

            if (!data) {
                console.log("game_status_update_req: data passed by client is null");
                return;
            }

            if (game.state != Game.GAME_STATE_ACTIVE) {
                console.log("Error: game is not active: " + game.state);
                return;
            }

            data.from = user.name; // add from field to payload

            // notify other users about game status update
            user.socket.broadcast.to(game.io_room).emit('game_status_update', data);
        });
    });

    // send game_start message
    console.log("Sending game_start for id:" + game.id);
    game.io.sockets.in(game.io_room).emit('game_start', {id: game.id});

    this.timer = setTimeout(function () {
        game.stop();
    }, Game.GUARD_TIMEOUT);

    // after get all final scores, stop the game
    this.waitForFinalScores().then(function () {
        process.nextTick(function(){game.stop()});
    });
};

Game.prototype.stop = function () {

    var game = this;

    if (game.state == Game.GAME_STATE_DONE) // game already stopped; nothing to do
        return;

    console.log("Game " + game.id + " shutting down...");

    // calculate user rankings (populates userStatsArray)
    this.computeStats();

    if (this.timer)
        clearTimeout(this.timer);

    console.log("Game "+game.id+":  sending 'game_end' message to all");
    this.io.sockets.in(this.io_room).emit('game_end', {
        id: this.id,
        userArray: game.userStatsArray
    });

    process.nextTick(function () {
        game.emit('end', null, game.userStatsArray);  // notify Room that game is done (Room will get event after some time)
    });

    this.state = Game.GAME_STATE_DONE;
    console.log("Set game " + this.id + " state to " + this.state);

    // remove all users from socket.io room
    this.gameUserDataSet.forEach(function (gameUserData) {
        gameUserData.user.socket.leave(game.io_room);
        console.log(gameUserData.user.name + " left socket-io room: '" + game.io_room + "'");

        // do not listen for 'game_status_update_req' and 'score_final' events anymore
        gameUserData.user.socket.removeAllListeners('game_status_update_req');
        gameUserData.user.socket.removeAllListeners('score_final');
    });

    // remove all users from set
    this.gameUserDataSet.clear();

};

Game.prototype.leave = function (user) {
    var game = this;
    filteredArray = this.gameUserDataSet.filter(function(x){return x.user == user});
    if (filteredArray.length > 0) {
        var gameUserData = filteredArray[0];
        console.log("Game "+this.id+": "+user.name+" left");
        // remove user from set
        this.gameUserDataSet.remove(gameUserData);
        console.log(user.name + " left game " + this.id);

        // if no more users left in game, stop it
        if (this.gameUserDataSet.count() <= 0)
            process.nextTick(function(){game.stop()});
    }
};

Game.prototype.waitForFinalScores = function () {

    var game = this;
    var deferred = Q.defer();

    // have all users submitted their final score ?
    var gotAllFinalScores = function () {
        return game.gameUserDataSet.size &&
            game.gameUserDataSet.all(function (gameUserData) {
                return gameUserData.gotFinalScore;
            });
    };

    this.gameUserDataSet.forEach(function (gameUserData) {
        var user = gameUserData.user;

        user.socket.once('score_final', function (data) {

            console.log("Received score_final for " + user.name);
            gameUserData.gotFinalScore = true;

            if (game.state != Game.GAME_STATE_ACTIVE) {
                console.error("Error: game over;  score_final message sent by " + user.name + " not expected.");
                return;
            }

            if (!(data && data.score && data.time && data.stats.qAnswered && data.stats.qAnsweredCorrectly && data.endStatus)){
                console.error(user.name + " sent invalid score_final.  kick user out of room." + JSON.stringify(data));
                user.room.leave(user);
                return;
            }

            gameUserData.score = data.score;
            gameUserData.time = data.time;
            gameUserData.qAnswered = data.stats.qAnswered;
            gameUserData.qAnsweredCorrectly = data.stats.qAnsweredCorrectly;
            gameUserData.endStatus = data.endStatus;

            // check if final score rcvd for all users
            if (gotAllFinalScores())
                deferred.resolve();

        });

        user.socket.once('disconnect', function () {
            // check if final score rcvd for all users
            if (gotAllFinalScores()) {
                deferred.resolve();
            }
        });

    });

    return deferred.promise;
};

Game.prototype.computeStats = function () {
    var game = this;

    var usersFinished = [];
    var usersDNF = [];

    this.gameUserDataSet.forEach(function (gameUserData) {

        if (!gameUserData.gotFinalScore) // don't include any users who haven't sent final score in rankings
            return;

        var userStats = {
            id: gameUserData.user.name,
            finishedPosition: -1,
            score: gameUserData.score,
            time: gameUserData.time,
            stats: {
                qAnswered: gameUserData.qAnswered,
                qAnsweredCorrectly: gameUserData.qAnsweredCorrectly
            },
            endStatus: gameUserData.endStatus,
            cupPoints: -1
        };

        if (userStats.score == game.TARGET_POINTS){ // user finished race
            usersFinished.push(userStats)
        } else {                                    // user did not finish race
            usersDNF.push(userStats);
        }
    });

    // sort userFinished by time (ascending by time)
    usersFinished.sort(function(a,b){return a.time - b.time;});

    // sort useresDNF by score (descending by score)
    usersDNF.sort(function(a,b){return b.score - a.score;});

    // set rankings for finished users (if 2 users have the same time, keep their ranking the same)
    var ranking = 0;
    var prevTime = null;
    for (i=0; i<usersFinished.length; i++){
        var user = usersFinished[i];

        // if users have the same time, keep their ranking the same
        if (prevTime != user.time){
            ranking++;
        }

        user.finishedPosition = ranking;
        user.cupPoints = game.rankingToCupPoints(ranking);
        prevTime = user.time;
    }

    // set rankings for DNF users (if 2 users have same score, keep their ranking the same)
    var prevScore = null;
    for (i=0; i<usersDNF.length; i++) {
        var user = usersDNF[i];

        if (prevScore != user.score){
            ranking++;
        }

        user.finishedPosition = ranking;
        user.cupPoints = game.rankingToCupPoints(ranking);
        prevScore = user.score;
    }

    // merge usersFinished, usersDNF into one array
    game.userStatsArray = usersFinished.concat(usersDNF);
};

Game.prototype.rankingToCupPoints = function(ranking){
    var cupPoints = 0;
    switch(ranking){
        case 1:
            cupPoints = 10;
            break;
        case 2:
            cupPoints = 7;
            break;
        case 3:
            cupPoints = 5;
            break;
        case 4:
            cupPoints = 3;
            break;
        case 5:
            cupPoints = 2;
            break;
        case 6:
            cupPoints = 1;
            break;
        default:
            cupPoints = 1;
            break;
    }
    return cupPoints;
};