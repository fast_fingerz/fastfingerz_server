function TestUser(serverAddr){
    this.id = this.getRandomInt(0,1000000);
    this.name = "test_"+this.id;
    this.serverAddr = serverAddr;
    this.logger = function(msg){console.error("logger not set")};
    this.onConnectCB = function(){};
    this.onDisconnectCB = function(){};
    this.scoreUpdateIntervalObj = null;
    this.socket = null;
}

TestUser.prototype.setLogger = function(logger){
    this.logger = logger;
};

TestUser.prototype.onConnect = function(onConnectCB){
    this.onConnectCB = onConnectCB;
};

TestUser.prototype.onDisconnect = function(onDisconnectCB){
    this.onDisconnectCB = onDisconnectCB;
};

gTemp = 0;

TestUser.prototype.login = function(){

    var temp = gTemp;
    gTemp++;

    var testuser = this;

    testuser.log("=== "+this.name+" ===");

    testuser.socket = io.connect(this.serverAddr, {
        transports: ['websocket'],
        query: "token="+this.name+
               "&logintype=userid"+
               "&friendlyname="+"User Dispay Name "+this.id+
               "&avatarid="+this.id,
        'force new connection': true
    });

    testuser.socket.on('connect', function () {
        testuser.log("websocket connected!");
        testuser.onConnectCB(); // notify testuser's client that connection was established
    });

    testuser.socket.on('game_data', function(data){
        testuser.logrx("game_data:  "+JSON.stringify(data));
    });

    testuser.socket.on('game_start', function (data) {
        testuser.logrx("game_start:  " + JSON.stringify(data));

        // send score updates in 'game_status_update_req' a simulated number of times, followed by 'score_final'
        var count = 0;
        if (temp%2 == 0)
            scoreupdateinterval = 0;
        else
            scoreupdateinterval = 2000;

        testuser.scoreUpdateIntervalObj = setInterval(function () {
            if (count >= 3) {
                clearInterval(testuser.scoreUpdateIntervalObj);
                var randomDelay = testuser.getRandomInt(5000,6000);
                if (temp%2 == 0) randomDelay = 50;
                testuser.log("Wait random "+randomDelay+" ms before sending final score");
                testuser.finalScoreDelayTimer = setTimeout(function(){
                    var score_final_params = {score: 1000, time: randomDelay, stats: {qAnswered:15 ,qAnsweredCorrectly:10}, endStatus: "targetScorereached"};
                    testuser.log("score_final: "+JSON.stringify(score_final_params));
                    testuser.socket.emit('score_final', score_final_params);
                }, randomDelay);
                return;
            }

            count++;

            var game_status_update_req_params = {type:"score", payload: count * 50 + testuser.id};
            testuser.log("game_status_update_req:  "+JSON.stringify(game_status_update_req_params));
            testuser.socket.emit('game_status_update_req', game_status_update_req_params);

        }, scoreupdateinterval);

    });

    testuser.socket.on('game_end', function(data){
        testuser.logrx("game_end:  "+JSON.stringify(data));

        if (testuser.scoreUpdateIntervalObj)
            clearInterval(testuser.scoreUpdateIntervalObj);

    });

    testuser.socket.on('game_status_update', function(data){
        testuser.logrx("game_status_update:  "+JSON.stringify(data));
    });

    testuser.socket.on('disconnect', function () {
        testuser.logrx("websocket disconnected");
        if (testuser.gameDataAckTimer)
            clearTimeout(testuser.gameDataAckTimer);

        if (testuser.finalScoreDelayTimer)
            clearTimeout(testuser.finalScoreDelayTimer);

        if (testuser.scoreUpdateIntervalObj)
            clearTimeout(testuser.scoreUpdateIntervalObj);

        testuser.onDisconnectCB();
    });

    testuser.socket.on('error', function (err) {
        testuser.logrx("websocket error: " + err);
    });

    testuser.socket.on('room_user_joined', function(data) {
        testuser.logrx("room_user_joined: "+JSON.stringify(data));
        userInfo = data;

        testuser.logrx("User "+userInfo.id+" joined room    (displayName='"+userInfo.displayName+"'   avatarId="+userInfo.avatarId+")");
    });

    testuser.socket.on('room_user_left', function(data) {
        testuser.logrx("room_user_left: "+JSON.stringify(data));
    });

    testuser.socket.on('room_ready', function(data) {
        testuser.logrx("room_ready: "+JSON.stringify(data));
    });
    testuser.socket.on('room_end', function (data) {
        testuser.logrx("room_end: " + data);
        //testuser.socket.disconnect(); //exit after playing 1 game session
    });

    testuser.socket.on('game_data', function(data, cb){
        var randomDelay = testuser.getRandomInt(0,1000);
        testuser.log("Wait random "+randomDelay+" ms before sending ACK for game_data");
        testuser.gameDataAckTimer = setTimeout(function(){
            testuser.log("game_data rsp:  ok");
            cb("ok");
        },randomDelay);
    });

    testuser.socket.on('cup_summary', function(data){
        testuser.logrx("cup_summary:  "+JSON.stringify(data));
    });

};

TestUser.prototype.joinRoom = function(params){

    var testuser = this;

    if (!(testuser.socket && testuser.socket.connected)){
        console.error("join: "+testuser.id + " socket not connected");
        return;
    }

    var join_room_params = params || {raceType:"single_race"};
    testuser.log("join_room  "+JSON.stringify(join_room_params));
    testuser.socket.emit('join_room', join_room_params, function(rspData){
        testuser.logrx("join_room rsp: "+JSON.stringify(rspData));
        roomJoinRsp = rspData;
        if (roomJoinRsp.err){
            testuser.logrx("Failed to join room; err="+roomJoinRsp.err);
        }
        else {
            var userInfoList = roomJoinRsp.data.users;
            userInfoStringList = userInfoList.map(function(s){
                return "(" + s.id + ", " + s.displayName + ", " + s.avatarId + ")";
            });
            var userInfoStr = "["+ userInfoStringList.join() + "]";

            testuser.logrx("Successfully joined room;  Users="+userInfoStr);
        }

    });
};

TestUser.prototype.leaveRoom = function(){

    var testuser = this;

    if (!(testuser.socket && testuser.socket.connected)){
        console.error("join: "+testuser.id + " socket not connected");
        return;
    }

    testuser.log("leave_room");
    testuser.socket.emit('leave_room', "", function(rspData){
        testuser.logrx("leave_room rsp: "+JSON.stringify(rspData));
        var rsp = JSON.parse(rspData);
        if (rsp.err){
            testuser.logrx("failed to leave room: err="+rsp.err);
        }
        else{
            testuser.logrx("successfully left room: roomId="+rsp.roomId);
        }

    });
};

TestUser.prototype.kill = function(){
    if (this.socket)
        this.socket.disconnect();
};

TestUser.prototype.log = function(msg){
    this.logger(msg);
};

TestUser.prototype.logrx = function(msg){
    this.logger("====>\t\t"+msg);
};

TestUser.prototype.getRandomInt = function(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};