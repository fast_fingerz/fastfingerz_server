var module = angular.module("testApp",['ngRoute']);

module.config(function($routeProvider){
    $routeProvider.when('/mainView', {
        templateUrl: '/partials/main.html',
        controller: 'MainController'
    })
    .otherwise({ redirectTo: '/mainView' });
});

module.controller('MainController', function($scope, $sce) {

    var userSet = new Set();

    $scope.url = "localhost:3000";
    $scope.numberUsersToSpawn = 1;
    $scope.activeUsers = 0;
    $scope.colorful = true;
    $scope.cup = false;
    $scope.autoJoin = true;
    $scope.spawn = function(){

        // spawn required number of new users with given parameters
        for (i=0; i< $scope.numberUsersToSpawn; i++){

            // generate a random color for user
            var color = randomColor();

            var createLogger = function(color, name){
                return function(msg){
                    var now = new Date;
                    msg = ("0"+now.getHours()).slice(-2) + ":" + ("0"+now.getMinutes()).slice(-2) + ":" + ("0"+now.getSeconds()).slice(-2) + "." + ("00"+now.getMilliseconds()).slice(-3) + "\t\t" + name + ":\t\t" + msg;

                    if ($scope.colorful) {
                        $("#idLogArea").append("<span style='color:" + color + "'>" + msg + "</span><br/>");
                    }
                    else {
						console.log(msg);
					}
                };
            };

            var createOnConnectHandler = function (user) {
                return function () {
                    if ($scope.autoJoin)
                        user.joinRoom(getJoinRoomParams());

                    $scope.$apply(function () {
                        $scope.activeUsers++;
                    });
                }
            };

            var user = new TestUser($scope.url);
            user.setLogger(createLogger(color,user.name));
            user.onConnect(createOnConnectHandler(user));
            user.onDisconnect(function(){
                $scope.$apply(function(){
                    $scope.activeUsers--;
                });
            });

            user.login();

            userSet.add(user);
        }

    };

    $scope.killAll = function() {
        setTimeout(function() {
            userSet.forEach(function (user) {
                user.kill();
            });
            userSet.clear();
        },0);
    };

    $scope.joinRoom = function() {
        setTimeout(function() {
            userSet.forEach(function (user) {
                user.joinRoom(getJoinRoomParams());
            });
        },0);
    };

    $scope.leaveRoom = function() {
        setTimeout(function() {
            userSet.forEach(function (user) {
                user.leaveRoom();
            });
        },0);
    };

    $scope.clear = function(){
        $("#idLogArea").empty();
    };

    function getJoinRoomParams(){
        return {raceType:$scope.cup?"cup":"single_race"};
    }

});