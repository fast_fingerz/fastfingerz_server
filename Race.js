var util         = require("util");
var EventEmitter = require("events").EventEmitter;
var Set = require('jsclass/src/set').Set;
var async = require("async");
var Q = require('q');

module.exports = Race;

// constants
Race.GAME_DATA_ACK_TIMEOUT = 5000;

function Race(){
    EventEmitter.call(this);
}
util.inherits(Race, EventEmitter);

Race.prototype.sendGameData = function (users, gameDataPayload) {
    var race = this;
    var deferred = Q.defer();

    var timeoutTimer = setTimeout(function () {
        deferred.reject(new Error("timeout waiting for game_data acks"));
    }, Race.GAME_DATA_ACK_TIMEOUT);

    var readyUsersSet = new Set(race.usersSet);
    async.each(users, function (user, cb) {

        user.socket.on('disconnect', function () {
            readyUsersSet.remove(user);
            notify(cb);
        });

        user.socket.emit("game_data", gameDataPayload, function (ack) {
            console.log("Got ack for game_data from " + user.name);
            readyUsersSet.add(user);
            notify(cb);
        });

        var notified = false;
        function notify(fn) {
            if (!notified) {
                notified = true;
                fn();
            }
        }

    }, function (err) {
        if (readyUsersSet.size)
            deferred.resolve();
        else // no users are ready
            deferred.reject(new Error("Race: No users ready"));
    });

    return deferred.promise;
};